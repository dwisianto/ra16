
dbclear all
clear,clc
addpath('F:\\DCw\\DBb\\ra16\\tb\\mvolkovs\\y10liblinear\\matlab\\')
dbstop evaluate_model

loc_in   = 'F:\\DDb\\rk\\letor4\\mq8mb\\Fold1\\';
name_arr = ["train.liblinearmat";"test.liblinearmat";"valid.liblinearmat"];
for i=1:3
  load([loc_in, name_arr(i,:)])
end



% fminunc
trn_dat = sparse(trn_dat);
trn_lbl = trn_lbl;
model=train(trn_lbl, trn_dat, '-s 0 -c 10000000001');
[l,a,b]=predict(trn_lbl, trn_dat, model, '-b 1');


%file_path='F:\\DDb\\rk\\letor4\\mq8tmp\\Fold1\\';
file_path='F:\DDb\rk\letor4\mq8tmp\Fold1\';
data_path='F:\DDb\rk\letor4\';
scores=b(:,1);
dataset='mq8b';
fold=1;
which='train';
[ndcg, precision, map] = evaluate_model(file_path, data_path, scores, dataset, fold, which)



