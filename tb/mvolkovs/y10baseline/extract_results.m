function [ndcg, precision, map] = extract_results(file_name)

precision = zeros(1, 10);
ndcg = zeros(1, 10);
map = zeros(1, 1);

results = textread(file_name, '%s');
	
map(1, 1) = str2num(results{24, 1});
for i = 1:10
	precision(1, i) = str2num(results{13+i, 1});
	ndcg(1, i) = str2num(results{38+i, 1});
end


end



