function [ _dOut, _lOut ] = dat_combine ( _dat, _lbl)

_dOut = [];
for i=1:size( _dat,1)
  _dOut = [ _dOut; _dat{i} ];
end
%size( dOut )

_lOut=[];
for i=1:size( _lbl,1)
  _lOut = [ _lOut; _lbl{i} ];
end

endfunction
