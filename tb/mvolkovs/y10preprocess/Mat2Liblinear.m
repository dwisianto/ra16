
dbclear all
clear,clc
addpath('F:\\DCw\\DBb\\ra16\\tb\\mvolkovs\\y10liblinear\\matlab\\')
dbstop Mat2Liblinear 14


loc_in   = 'F:\\DDb\\rk\\letor4\\mq8mb\\Fold1\\';
name_arr = ["train";"test";"valid"];
for i=1:3
  load([loc_in, name_arr(i,:)])
end

[trn_dat, trn_lbl] = dat_combine ( train_data, train_targets );
[val_dat, val_lbl] = dat_combine ( valid_data, valid_targets );
[tst_dat, tst_lbl] = dat_combine ( test_data, test_targets );
clear train_data train_targets
clear test_data test_targets
clear valid_data valid_targets

trn_lbl(trn_lbl<0.5) = -1;
trn_lbl(trn_lbl>0.5) = 1;

tst_lbl(tst_lbl<0.5) = -1;
tst_lbl(tst_lbl>0.5) = 1;

val_lbl(val_lbl<0.5) = -1;
val_lbl(val_lbl>0.5) = 1;



loc_out = loc_in;
save([loc_out, 'train.liblinearmat'], 'trn_dat', 'trn_lbl' );
save([loc_out, 'test.liblinearmat'], 'tst_dat', 'tst_lbl' );
save([loc_out, 'valid.liblinearmat'], 'val_dat', 'val_lbl' );
