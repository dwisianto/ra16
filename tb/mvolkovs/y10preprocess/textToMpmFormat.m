function textToMpmFormat(in_loc, in_name, out_loc, out_name)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% For a given tab delimited file, conver the file into MPM readable format
%
% Note:
%   - missing value is NULL
%
% yuanyuan.li@nih.gov
% created : 05-15-2015
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
addpath(genpath('/media/sdb1/tb/ma_bi/'))

disp('- Read in text files');
%%%%%%%%%%%%%%%%%%   Read data   %%%%%%%%%%%%%%%%%%%%%%%%%
% Read in the text file
filename=[in_loc,in_name];

% Open file & get the first line
fid=fopen(filename);
line=fgetl(fid);
fclose(fid);

tmp=textscan(line,'%s');
% the length of tmp gives how many columns (fields)
n=numel(tmp{1});

% Now read the file
fid=fopen(filename,'r');
% First column is gene name, rest are RNAseq values
fmt=repmat('%f\t', [1,n-1]);   fmt=['%s\t', fmt];
dat=textscan(fid,fmt,'EmptyValue',NaN);
fclose(fid);


disp('- Convert to MPM format');
%%%%%%%%%%%%%%%%%% Convert it to MPM format %%%%%%%%%%%%%%%%%%%%%%
fid=fopen([out_loc, out_name],'w');
for i=1:length(classlbl)
    % col1: class label, col2:sampleID
    fprintf(fid,'%d\tqid:%d', classlbl(i), i);
    % for each expression value column add col#: in the front
    for ii=1:numel(dat{1})
        if isnan(dat{i+1}{ii}==FALSE)
            fprintf(fid,'\t%d:%s', ii, dat{i+1}{ii});
        else
            fprintf(fid,'\t%d:%s', ii, 'NULL');
        end%end-if-else
    end%end-for each column

    fprintf(fid,'\n');
end%end-for each sample
fclose(fid);


save([out_loc, out_name,'.mat']);
 
disp('done');
end%end-function