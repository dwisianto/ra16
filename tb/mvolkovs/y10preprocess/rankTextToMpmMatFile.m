function [dat lbl] = rankTextToMpmMatFile(in_loc,in_name,out_name,out_loc)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% For a given rank text file, conver the file into MPM readable format
%   - 0 qid:3 1:56 2:204 3:204 4:142 5:140 6:189 7:182 8:183 
% Note:
%   - missing value is NULL
%   rankTextToMpmMatFile('/media/sdb1/db/rankPreference/', 'test_txt_aggregate.txt', 'test_txt_aggregate','/media/sdb1/gb/po/pancancer_po/s0901/Do/');
%
% yuanyuan.li@nih.gov
% created : 05-15-2015
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
addpath(genpath('/media/sdb1/tb/ma_bi/'))

%%%%%%%%%%%%%%%%%%   Read data   %%%%%%%%%%%%%%%%%%%%%%%%%
disp('- Read in text file');

% Read in the text file
filename=[in_loc,in_name];

% Open file & get the first line
fid=fopen(filename);
line=fgetl(fid);
fclose(fid);

tmp=textscan(line,'%s');
% the length of tmp gives how many columns (fields)
n=numel(tmp{1});

% Now read the file
fid=fopen(filename,'r');
% First column is gene name, rest are RNAseq values
fmt=repmat('%s\t', [1,n-1]);   fmt=['%f\t', fmt];
dat=textscan(fid,fmt,'EmptyValue',NaN);
fclose(fid);

% 1st col = rank label
l=dat{1};

% 2nd col = question ID   FORMAT: qid:xx    
% Note: use CStr2String instead of sprintf if file is large
str=sprintf('%s;', dat{2}{:});  % convert to a single string
qid=strread(str, 'qid:%f;');    % get numeric part only

fea_len=numel(dat)-2; % number of feature

d = zeros(length(qid), fea_len);
cnt=1;
% for each data columns, get the data (2nd numeric value after ':')
for i=3:numel(dat) 
    str=sprintf('%s;', dat{i}{:});  % convert to a single string
    [x d(:,cnt)] =strread(str, '%f:%f;');  % get the 2nd numeric part only 
    cnt=cnt+1;
end%end-for

clear dat; clear str; clear x;


%%%%%%%%%%%%%%%%%% Convert it to MPM format %%%%%%%%%%%%%%%%%%%%%%
disp('- Convert to MPM format');
qid_uniq=unique(qid);
sam_len=length(qid_uniq);

% allocate cell 
dat={};
lbl={};

% Group data & label based on each qid
for i=1:sam_len
    idx=find(qid==qid_uniq(i));
    dat{i}=d(idx,:);
    lbl{i}=l(idx,:);
end%end-for

clear d; clear l; clear qid;

save([out_loc, out_name,'_dat.mat'], 'dat');
save([out_loc, out_name,'_lbl.mat'], 'lbl');

%find thetas using labeled training queries
thetas = find_thetas_supervised(lbl, dat)';


 
disp('done');
end%end-function