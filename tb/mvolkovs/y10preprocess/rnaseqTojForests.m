function rnaseqTojForests(in_loc,in_name,out_name,out_loc)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% in_loc='/media/sdb1/db/SKCM/RandomForrest/';
% in_name='all_skcm_rnaseq_cutaneous_lymph_metastatic';
% rnaseqTojForests(in_loc,in_name,in_name,in_loc)
% yuanyuan.li@nih.gov
% created : 02-19-2015
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
addpath(genpath('/media/sdb1/tb/ma_bi/'))

disp('- Read in .class & .value files');
%%%%%%%%%%%%%%%%%%   Read data   %%%%%%%%%%%%%%%%%%%%%%%%%
% Read in .class (1st column is sample name)
filename=[in_loc,in_name,'.class'];
classlbl=dlmread(filename,'\t',0,1);

% Read in .value
filename=[in_loc,in_name,'.value'];
% Open file & get the first line
fid=fopen(filename);
line=fgetl(fid);
fclose(fid);

tmp=textscan(line,'%s');
% the length of tmp gives how many lines
n=numel(tmp{1});

% Now read the file
fid=fopen(filename,'r');
dat=textscan(fid,repmat('%s\t', [1,n]),'EmptyValue',NaN);
fclose(fid);
gene=dat{1};


disp('- Convert to jforrest format');
%%%%%%%%%%%%%%%%%% Convert it to jForests format %%%%%%%%%%%%%%%%%%%%%%
fid=fopen([out_loc, out_name, '_jfmt.txt'],'w');
for i=1:length(classlbl)
    % col1: class label, col2:sampleID
    fprintf(fid,'%d\tqid:%d', classlbl(i), i);
    % for each expression value column add col#: in the front
    for ii=1:numel(gene)
        fprintf(fid,'\t%d:%s', ii, dat{i+1}{ii});
    end%end-for each column

    fprintf(fid,'\n');
end%end-for each sample
fclose(fid);

save([out_loc, out_name,'.mat']);
 
disp('done');
end%end-function
